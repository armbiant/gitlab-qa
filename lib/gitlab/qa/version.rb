# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '8.15.3'
  end
end
