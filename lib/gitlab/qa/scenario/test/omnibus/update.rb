# frozen_string_literal: true

require 'tmpdir'
require 'fileutils'

module Gitlab
  module QA
    module Scenario
      module Test
        module Omnibus
          class Update < Scenario::Template
            def perform(from_release, to_release = nil, *rspec_args)
              version = Component::Gitlab.perform do |gitlab|
                gitlab.release = from_release
                gitlab.act do
                  pull
                  package_version
                end
              end

              if version
                # Sub any plus symbols in version eg. "15.1.2+ce.0"
                version.tr!('+', '-')
                Runtime::Logger.info("Found previous version '#{version}'")

                current_release = QA::Release.new(to_release || from_release)

                type = current_release.ee? ? 'ee' : 'ce'

                existing_previous = Component::Gitlab.perform do |gitlab|
                  gitlab.act do
                    next "gitlab/gitlab-#{type}:#{version}" if exist?("gitlab/gitlab-#{type}", version)

                    next "gitlab/gitlab-#{type}:#{version}-#{type}.0" if exist?("gitlab/gitlab-#{type}", "#{version}-#{type}.0")

                    nil
                  end
                end
              else
                Runtime::Logger.info("Could not find previous image version")
                existing_previous = nil
              end

              previous_release = if existing_previous
                                   Runtime::Logger.info("Using previous image '#{existing_previous}'")
                                   QA::Release.new(existing_previous)
                                 else
                                   Runtime::Logger.info("Using stable as previous image")
                                   QA::Release.new(from_release).previous_stable
                                 end

              Docker::Volumes.new.with_temporary_volumes do |volumes|
                Component::Gitlab.perform do |gitlab|
                  gitlab.release = previous_release
                  gitlab.volumes = volumes
                  gitlab.network = 'test'
                  gitlab.seed_admin_token = false
                  gitlab.launch_and_teardown_instance
                end

                Scenario::Test::Instance::Image
                  .perform(current_release, *rspec_args) do |scenario|
                  scenario.volumes = volumes
                end
              end
            end
          end
        end
      end
    end
  end
end
