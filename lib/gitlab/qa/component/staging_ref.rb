# frozen_string_literal: true

module Gitlab
  module QA
    module Component
      class StagingRef < Staging
        ADDRESS = 'https://staging-ref.gitlab.com'
        GEO_SECONDARY_ADDRESS = 'https://geo.staging-ref.gitlab.com'
      end
    end
  end
end
