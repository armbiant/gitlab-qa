# Run QA tests against a remote Selenium grid

The QA tests have the ability to be run against a local or remote grid.

I.e, if you have a Selenium server set up at http://localhost:4444 or if you have a SauceLabs / BrowserStack account.

## Variables

| Variable                  | Description                                                    | Default  | Example(s)                     |
|---------------------------|----------------------------------------------------------------|----------|--------------------------------|
| QA_BROWSER                | Browser to run against                                         | "chrome" | "chrome" "firefox" "safari"    |
| QA_REMOTE_GRID_PROTOCOL   | Protocol to use                                                | "http"   | "http" "https"                 |
| QA_REMOTE_GRID            | Remote grid to run tests against                               |          | "localhost:3000" "provider:80" |
| QA_REMOTE_GRID_USERNAME   | Username to specify in the remote grid. "USERNAME@provider:80" |          | "gitlab-sl"                    |
| QA_REMOTE_GRID_ACCESS_KEY | Key/Token paired with `QA_REMOTE_GRID_USERNAME`                |          |                                |
| QA_REMOTE_TUNNEL_ID       | Name of the remote tunnel to use                               | "gitlab-sl_tunnel_id" |                                |
| QA_REMOTE_MOBILE_DEVICE_NAME | Name of mobile device to test against. `QA_BROWSER` must be set to `safari` for iOS devices and `chrome` for Android devices.                                                |          | "iPhone 12 Simulator"          |

## Testing with Sauce Labs

Running directly against an environment like staging is not recommended because test logs expose credentials. Therefore, it is best practice and the default to use a tunnel.

To install a tunnel, follow these [instructions](https://docs.saucelabs.com/secure-connections/sauce-connect/installation). 

To start the tunnel, copy the run command in **Sauce Labs > Tunnels** and run it in the terminal. You must be logged in to Sauce Labs. Use the credentials in 1Password to log in.

It is highly recommended to use `GITLAB_QA_ACCESS_TOKEN` to speed up tests and reduce flakiness.


### Run a test in a desktop browser

While tunnel is running, to test against a local instance in a desktop browser, run:
```shell
$ QA_BROWSER="safari" \
  QA_REMOTE_GRID="ondemand.saucelabs.com:80" \
  QA_REMOTE_GRID_USERNAME="gitlab-sl" \
  QA_REMOTE_GRID_ACCESS_KEY="<access key found in Sauce Lab account>" \
  GITLAB_QA_ACCESS_TOKEN="<token>" \
  gitlab-qa Test::Instance::Any <CE|EE> http://<local_ip>:3000 -- -- <relative_spec_path>
```

### Run a test in a mobile device browser

`QA_REMOTE_MOBILE_DEVICE_NAME` can be any device name in the [supported browser devices](https://saucelabs.com/platform/supported-browsers-devices) in the Emulators/simulators list, and the latest versions of Android or iOS. You must set `QA_BROWSER` to `safari` for iOS devices and `chrome` for Android devices.```

```shell
$ QA_BROWSER="safari" \
  QA_REMOTE_MOBILE_DEVICE_NAME="iPhone 12 Simulator" \
  QA_REMOTE_GRID="ondemand.saucelabs.com:80" \
  QA_REMOTE_GRID_USERNAME="gitlab-sl" \
  QA_REMOTE_GRID_ACCESS_KEY="<found in Sauce Lab account>" \
  GITLAB_QA_ACCESS_TOKEN="<token>" \
  gitlab-qa Test::Instance::Any <CE|EE> http://<local_ip>:3000 -- -- <relative_spec_path>
```

Results can be watched in real time in Sauce Labs under AUTOMATED > Test Results
